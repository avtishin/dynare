The Dynare Reference Manual, version |version|
==============================================

Currently the development team of Dynare is composed of:

* Stéphane Adjemian (Université du Maine, Gains)
* Houtan Bastani (CEPREMAP)
* Michel Juillard (Banque de France)
* Sumudu Kankanamge (Toulouse School of Economics)
* Frédéric Karamé (Université du Maine, Gains and CEPREMAP)
* Dóra Kocsis (CEPREMAP)
* Junior Maih (Norges Bank)
* Ferhat Mihoubi (Université Paris-Est Créteil, Érudite and CEPREMAP)
* Willi Mutschler (University of Münster)
* Johannes Pfeifer (University of Cologne)
* Marco Ratto (European Commission, Joint Research Centre - JRC)
* Sébastien Villemot (CEPREMAP)

The following people used to be members of the team:

* Abdeljabar Benzougar
* Alejandro Buesa
* Fabrice Collard
* Assia Ezzeroug
* Stéphane Lhuissier
* George Perendia

Copyright © 1996-2020, Dynare Team.

Permission is granted to copy, distribute and/or modify this document under the terms of the GNU Free Documentation License, Version 1.3 or any later version published by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.

A copy of the license can be found at `http://www.gnu.org/licenses/fdl.txt <http://www.gnu.org/licenses/fdl.txt>`__.

.. toctree::
   :numbered:
   :maxdepth: 4

   introduction
   installation-and-configuration
   running-dynare
   the-model-file
   the-configuration-file
   time-series
   reporting
   examples
   dynare-misc-commands
   bibliography

.. only:: builder_html

   Indices and tables
   ==================

   * :ref:`genindex`
